#
# Author: Ruben Figueroa y Alicia Franco
# Maintainers: Ruben Figueroa,Teresa Ruiz,Yoshua Pellman
# =======================================
# ENVIPE para IPECSID (Ruben Figueroa)
# 
rm(list = ls())
if(!require(pacman))install.packages("pacman")
p_load(dplyr, tidyverse, janitor, stringr, here, readxl, xlsx, ggplot2)

files = list(inp = here("process/output/datos-ifrasid.csv"),
             out = here("index/output/ifrasid-ent-14-19.csv"),
             graf = here ("index/graf/graf_ent_2014-2019.png"))

years <- c(2014:2019)

ifrasid_anual <- read.csv(file = files$inp) %>% 
  filter(is.na(nom_ent) == F) %>% 
  mutate(ent = formatC(ent, width = 2, flag = "0", format = "d"))

ifrasid <- data.frame()

for(i in 1:length(years)){

vars <- ifrasid_anual %>%  
  filter(year == years[i]) %>% 
  select(CIFNEG:PINSMU)

pca <- prcomp(vars, scale = TRUE)
tpc <- pca$x
sdev <- pca$sdev[1]
pc1 <- tpc[1:32,1]
ipecsid <- data.frame(round(pc1/sdev, 2)) %>% 
  mutate(year = years[i]) %>% 
  cbind(., data.frame(ent = c("01","02","03","04","05","06","07","08","09",10:32)))
ifrasid <- bind_rows(ifrasid,ipecsid)

rm(ipecsid)

}

ifrasid <- ifrasid %>% rename(ifrasid = round.pc1.sdev..2.) 
ifrasid_anual <- left_join(ifrasid_anual, ifrasid, by = c("year", "ent")) %>% 
  left_join(.,read_xlsx(paste0(here("import/"), "nom_ent.xlsx")) %>% 
              clean_names() %>% 
              mutate(ent = formatC(cve_ent, width = 2, flag = "0", format = "d")) %>% 
              select(-c(cve_ent)), 
            by = c("ent", "nom_ent"))

write.csv(ifrasid_anual, files$out)

tema <- theme_minimal() +
  theme(plot.title = element_text(size = 20, family = "Barlow Condensed", hjust = 0.5, face = "bold"),
        plot.subtitle = element_text(size = 16, family = "Barlow Condensed", hjust = 0.5),
        plot.caption = element_text(size = 10, family = "Barlow Condensed", hjust = 0, face = "italic"),
        axis.text = element_text(size = 16, family = "Barlow Condensed", color = "black"),
        axis.title = element_text(size = 14, family = "Barlow Condensed"),
        legend.text = element_text(size = 10, family = "Barlow Condensed", hjust = 0.5),
        legend.title = element_text(size = 10, family = "Barlow Condensed", hjust = 0.5),
        strip.text = element_text(size = 14, face = "bold", family = "Barlow Condensed"))

tempo <- ifrasid_anual %>% 
  select(nom_ent2, year, ifrasid) %>% 
  mutate(year = str_replace(year, "20", ""))

ggplot(tempo, aes(x = year, y = ifrasid, group = nom_ent2)) +
  geom_line(color = "#69409E") +
  geom_point(color = "#69409E") +
  facet_wrap(~ nom_ent2, nrow = 4) +
  labs(title = "Índice de Percepción Ciudadana Contra el Delito (IPECSID) por estado",
         subtitle = "2014-2019",
         x = "", 
         y = "IPECSID",
         caption = "Fuente: Elaboración propia del IPECSID a partir de la ENVIPE (2014-2019)") +
  tema +
  theme(axis.text.x = element_text(angle = 45))

ggsave(files$graf, width = 16, height = 12)  
