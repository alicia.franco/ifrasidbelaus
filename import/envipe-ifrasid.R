#
# Author: Alicia Franco
# Maintainers: Ruben Figueroa,Teresa Ruiz, Yoshua Pellman
# =======================================
# ENVIPE para IFRASID
# 

if(!require(pacman))install.packages("pacman")
p_load(dplyr, tidyverse, janitor, stringr, here, readxl)

#============ Directorios por modulo (PILA CON FACTOR) ================ #

dirs <- list(sdem = here("import/tsdem/"),
             percep = here("import/tper_vic/"),
             delito = here("import/tmod_vic/"),
             vivienda = here("import/tvivienda/"),
             inp = here("import/"),
             output = here("import/output/"))


# == Funciones útiles == #

id_maker <- function(x) {
  if (edicion <= 2014){
      x <- mutate(x, id_per = paste(control, viv_sel, hogar, r_sel, sep = "."))
    }
  else{
      x <- mutate(x, id_per = paste(upm, viv_sel, hogar, r_sel, sep = "."))
    }}

id_maker_dem <- function(x) {
  if (edicion == 2014){
      x <- mutate(x, id_per = paste(control, viv_sel, hogar, n_ren, sep = "."))
    }
  else{
      x <- mutate(x, id_per = paste(upm, viv_sel, hogar, n_ren, sep = "."))
    }}

# == PERCEPCIÓN == #

per_files <- dir(dirs$percep)
percepcion <- data.frame()

pb <- txtProgressBar(min = 1, max = length(per_files), style = 3)
for (i in 1:length(per_files)) {
  
  index <- substr(str_extract(per_files[i], "[^.]+"), 11,12)
  edicion <- 2000 + as.numeric(index)

  tempo <- readRDS(paste0(dirs$percep, per_files[1])) %>% 
    clean_names() %>% 
    id_maker() %>% 
    mutate(id_vivienda = paste(upm, viv_sel, sep = ".")) %>% 
    select(id_vivienda, ap5_4_02) %>% 
    select(id_vivienda, id_per, starts_with("ap5_3_"), starts_with("ap5_6_"), starts_with("ap5_4"), ap4_3_1:ap4_3_3,  fac_ele) %>% 
    select(id_vivienda, id_per, ap4_3_1:ap4_3_3, ends_with("02"), ends_with("03"), ends_with("06"), fac_ele) %>% 
    mutate(year = edicion,
           fac_ele = as.numeric(fac_ele)) #%>% 
    distinct(id_vivienda, .keep_all = T) 
  
  percepcion <- bind_rows(percepcion, tempo)
  rm(tempo)
  setTxtProgressBar(pb, i)
}
close(pb)
rm(pb, i)

# == DELITO == #

del_files <- dir(dirs$delito)
delito <- data.frame()

pb <- txtProgressBar(min = 1, max = length(del_files), style = 3)
for (i in 1:length(del_files)) {
  
  index <- substr(str_extract(del_files[i], "[^.]+"), 10,11)
  edicion <- 2000 + as.numeric(index)

  if (edicion != 2017){
   
     tempo <- readRDS(paste0(dirs$delito, del_files[i])) %>% 
      clean_names()  %>% 
      id_maker() %>% 
      select(id_per,bp1_23, bp1_27, bp1_20, bp1_21, bp1_24, bp1_2c, fac_del) %>% 
      mutate(year = edicion,
             fac_del = as.numeric(fac_del)) 
     }
  else{
    
    tempo <- readRDS(paste0(dirs$delito, del_files[i])) %>% 
      clean_names() %>% 
      id_maker() %>% 
      mutate(year = edicion,
             bp1_24 = case_when(bp1_24_1 == "1" | bp1_24_2 == "1" ~ "1",
                                bp1_24_1 == "2" | bp1_24_2 == "2" ~ "2",
                                bp1_24_1 == "9" | bp1_24_2 == "9" ~ "9", 
                                T ~ NA_character_),
             fac_del = as.numeric(fac_del)) %>% 
      select(id_per,bp1_23, bp1_27, bp1_20, bp1_21, bp1_24, bp1_2c, year, fac_del) 
    }  
  
  delito <- bind_rows(delito, tempo)
  setTxtProgressBar(pb, i)
}
close(pb)
rm(pb, i)


# == vivienda == #

viv_files <- dir(dirs$vivienda)
vivs <- data.frame()

pb <- txtProgressBar(min = 1, max = length(viv_files), style = 3)
for (i in 1:length(viv_files)) {

  index <- str_extract(viv_files[i], "(?<=-)[[:digit:]]+")
  edicion <- 2000 + as.numeric(index)

if(edicion < 2017){
  tempo <- readRDS(paste0(dirs$vivienda, viv_files[i])) %>% 
    clean_names() %>% 
    mutate(year = edicion,
           id_vivienda = paste(upm, viv_sel, sep = ".")) %>% 
    select(year, id_vivienda , ent) %>% 
    distinct(id_vivienda, .keep_all = T) 
}  
else{  
  tempo <- readRDS(paste0(dirs$vivienda,viv_files[i])) %>% 
    clean_names() %>% 
    mutate(year = edicion,
           id_vivienda = paste(upm, viv_sel, sep = ".")) %>% 
    rename(ent = cve_ent) %>% 
    select(year, id_vivienda , ent) %>% 
    distinct(id_vivienda, .keep_all = T) 
}
  
 vivs <- bind_rows(vivs, tempo)
 setTxtProgressBar(pb, i)
}  
 
 rm(tempo, index, edicion)

# Socio Dem
 import_sdem <- dir(dirs$sdem)
 sociodem14_19 <- data.frame()
 pb <- txtProgressBar(min = 1, max = length(import_sdem), style = 3)
 

 for (i in 1:length(import_sdem)) {
   
   index_name <- str_extract(import_sdem[i], "(?<=-)[[:digit:]]+")
   edicion <- 2000 + as.numeric(index_name)
   
   df_sociodem <- readRDS(paste0(dirs$sdem, import_sdem[i])) 
   
   df_sociodem <- df_sociodem %>% 
     clean_names() %>% 
     id_maker_dem() %>% 
     mutate(year = edicion) %>% 
     select(year, id_per,  edad) %>% 
     distinct(id_per, .keep_all = T) 
   
   sociodem14_19 <- bind_rows(sociodem14_19, df_sociodem)
   
   setTxtProgressBar(pb, i)
   
 }
 rm(pb, i)
 
# == JUNTADO TODO == #

clean_percepcion <- percepcion %>% 
  left_join(., sociodem14_19, by = c("year", "id_per")) %>% 
  left_join(., vivs, by = c("year", "id_vivienda")) %>% 
  left_join(., read_xlsx(paste0(dirs$inp, "nom_ent.xlsx")) %>% 
              clean_names() %>% 
              mutate(ent = formatC(cve_ent, width = 2, flag = "0", format = "d")) %>% 
              select(ent, nom_ent),
            by = "ent") %>% 
  filter(edad > 17)

saveRDS(clean_percepcion, paste0(dirs$output, "clean-percepcion.rds"))


clean_delito <- left_join(delito %>% rename(ent = bp1_2c), 
                          read_xlsx(paste0(dirs$inp, "nom_ent.xlsx")) %>% 
                            clean_names() %>% 
                            mutate(ent = formatC(cve_ent, width = 2, flag = "0", format = "d")) %>% 
                            select(ent, nom_ent),
                          by = "ent")
saveRDS(clean_delito, paste0(dirs$output, "clean-delito.rds"))

rm(percepcion, vivs, delito, del_files, per_files, viv_files, clean_delito, clean_percepcion, envipe)

a <- vivs %>% filter(ent == "09")
b <- left_join(a, percepcion, by = "id_vivienda") %>% 
  select(ap5_4_02)
